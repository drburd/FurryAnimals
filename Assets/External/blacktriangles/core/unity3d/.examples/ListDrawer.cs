//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace blacktriangles.ITEN
{
    [CustomPropertyDrawer( typeof(Recipe) )]
    public class RecipeInspector
    	: PropertyDrawer
    {
        // members /////////////////////////////////////////////////////////////
        private ReorderableList ingredientList              = null;

        private bool settingComplete                        = false;
        private int settingIndex                            = -1;
        private string settingString                        = System.String.Empty;

        // unity callbacks /////////////////////////////////////////////////////
        public override float GetPropertyHeight( SerializedProperty property, GUIContent label )
        {
            return 0.0f;
        }

        public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
        {
            btGui.OnGUI();
            SerializedProperty ingredientsProp = property.FindPropertyRelative( "ingredients" );

            if( ingredientList == null )
            {
                bool draggable = true,
                    displayHeader = true,
                    displayAddHeader = true,
                    displayRemoveButton = true;

                ingredientList = new ReorderableList( property.serializedObject
                                          , ingredientsProp
                                          , draggable
                                          , displayHeader
                                          , displayAddHeader
                                          , displayRemoveButton
                                        );

                ingredientList.drawHeaderCallback = OnDrawIngredientListHeader;
                ingredientList.drawElementCallback = OnDrawIngredientListElement;
                /*
                drawHeaderCallback
                drawElementCallback
                onReorderCallback
                onSelectCallback
                onAddCallback
                onAddDropdownCallback
                onRemoveCallback
                onCanRemoveCallback
                onChangedCallback
                */
            }

            ingredientList.DoLayoutList();
        }

        // list callbacks //////////////////////////////////////////////////////
        private void OnDrawIngredientListHeader( Rect position )
        {
            GUI.Label( position, "Ingredients" );
        }

        private void OnDrawIngredientListElement( Rect position, int index, bool isActive, bool isFocused )
        {
            const float kSearchButton = 25f;
            const float kCountSize = 50f;
            SerializedProperty ingredientProp = ingredientList.serializedProperty.GetArrayElementAtIndex( index );
            SerializedProperty itemIdProp = ingredientProp.FindPropertyRelative( "itemId" );
            SerializedProperty countProp = ingredientProp.FindPropertyRelative( "count" );

            // draw label
            Rect labelRect = new Rect( position.x, position.y, position.width, position.height );
            if( System.String.IsNullOrEmpty( itemIdProp.stringValue ) )
            {
                GUI.Label( labelRect, "[None]" );
            }
            else
            {
                GUI.Label( labelRect, itemIdProp.stringValue );
            }

            // search button
            Rect searchButtonRect = new Rect( position.width - kSearchButton + 2, position.y+1, kSearchButton - 5f, position.height - 5f );
            if( GUI.Button( searchButtonRect, EditorIcons.Search ) )
            {
                if( ItemSelector.isShowing == false )
                {
                    settingComplete = false;
                    settingIndex = index;
                    ItemSelector.SelectItem( new GUIContent( "Select Ingredient" ), (item)=>{
                            settingComplete = true;
                            settingString = item.itemId;
                        });
                }
            }

            if( settingComplete && settingIndex == index )
            {
                itemIdProp.stringValue = settingString;
                settingComplete = false;
                settingIndex = -1;
            }

            // count
            Rect countRect = new Rect( position.width, position.y + 2f, 35f, position.height - 5f );
            EditorGUI.PropertyField( countRect, countProp, GUIContent.none );
        }
    }
}
