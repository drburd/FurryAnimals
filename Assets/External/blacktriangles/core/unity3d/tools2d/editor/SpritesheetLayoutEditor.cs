//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using blacktriangles;

namespace blacktriangles.Tools2d
{
	public class SpritesheetLayoutEditor
		: EditorWindow
	{
		// constants //////////////////////////////////////////////////////////
		private const string kMenuPath 							= "Tools/blacktriangles/2d/Spritesheet Layout Editor";
		private const string kTitle								= "Spritesheet Layout Editor";

		// members ////////////////////////////////////////////////////////////
		private SpritesheetLayout layout						= new SpritesheetLayout();

		private Vector2 animationSelectorScroll					= Vector2.zero;
		private Vector2 frameSelectorScroll						= Vector2.zero;
		private Vector2 spriteSelectorScroll					= Vector2.zero;

		private GUIStyle buttonNormal							= null;
		private GUIStyle buttonNoBorder							= null;
		private GUIStyle listItemNormal							= null;
		private GUIStyle listItemHighlight						= null;

		private int selectedAnimationIndex						= -1;
		private int selectedFrameIndex							= -1;
		private SpritesheetAnimation selectedAnimation			{ get { return GetSelectedAnimation(); } }
		private SpritesheetAnimationFrame selectedFrame			{ get { return GetSelectedFrame(); } }

		private float previewAnimFPS							= 60f;
		private double lastUpdateTime							= 0;

		// constructor / initializer //////////////////////////////////////////
		[MenuItem( kMenuPath  )]
		public static void OpenWindow()
		{
			SpritesheetLayoutEditor window = EditorWindow.GetWindow<SpritesheetLayoutEditor>() as SpritesheetLayoutEditor;
			window.titleContent = new GUIContent( kTitle );
			window.Initialize();
		}

		private void Initialize()
		{
			layout = null;
			EditorIcons.LoadIcons();
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnEnable()
		{
			Initialize();
		}

		protected virtual void OnGUI()
		{
			RefreshGUIStyles();

			GUILayout.BeginHorizontal();
				// animation layout //
				GUILayout.BeginVertical( GUI.skin.box, GUILayout.Width( position.width / 4f ) );
					DoAnimationSelectionGUI();
				GUILayout.EndVertical();

				GUILayout.BeginVertical();
					// details //
					SpritesheetAnimation selAnim = selectedAnimation;
					if( selAnim != null )
					{
						GUILayout.BeginVertical( GUI.skin.box );
							DoAnimationDetailsGUI( selAnim );
						GUILayout.EndVertical();

						// selector //
						GUILayout.BeginVertical( GUI.skin.box );
							DoSpriteSelectorGUI();
						GUILayout.EndVertical();
					}
				GUILayout.EndVertical();
			GUILayout.EndHorizontal();
		}

		protected virtual void Update()
		{
			double now = EditorApplication.timeSinceStartup;
			if( now - lastUpdateTime > 1f/previewAnimFPS )
			{
				lastUpdateTime = now;
				Repaint();
			}
		}

		// private methods ////////////////////////////////////////////////////
		private SpritesheetAnimation GetSelectedAnimation()
		{
			SpritesheetAnimation result = null;
			if( layout != null && layout.animations.IsValidIndex( selectedAnimationIndex ) )
			{
				result = layout.animations[ selectedAnimationIndex ];
			}

			return result;
		}

		private SpritesheetAnimationFrame GetSelectedFrame()
		{
			SpritesheetAnimationFrame result = null;
			SpritesheetAnimation selAnim = selectedAnimation;
			if( selAnim != null && selAnim.frames.IsValidIndex( selectedFrameIndex ) )
			{
				result = selAnim.frames[ selectedFrameIndex ];
			}

			return result;
		}

		// gui callbacks //////////////////////////////////////////////////////
		private void RefreshGUIStyles()
		{
			if( listItemNormal == null )
			{
				GUIStyles.Initialize();
				GUIStyles.OnGUI();
			}

			if( listItemNormal == null )
				listItemNormal = GUIStyles.buttonNormal;

			if( listItemHighlight == null )
				listItemHighlight = GUIStyles.buttonHighlight;

			if( buttonNormal == null )
				buttonNormal = GUIStyles.buttonNormal;

			if( buttonNoBorder == null )
				buttonNoBorder = GUIStyles.buttonNoBorder;
		}

		private void DoAnimationSelectionGUI()
		{
			if( layout == null )
			{
				GUILayout.Label( "Animation Selection" );
				GUILayout.Label( "No animation layout." );
  				GUILayout.Label( "Press 'Load' or 'New' button to work on one." );
				GUILayout.FlexibleSpace();
			}
			else
			{
				GUILayout.BeginHorizontal( GUI.skin.box );
					GUILayout.Label( "Animation Selection" );
					if( GUILayout.Button( EditorIcons.Plus, buttonNormal, GUILayout.Width( 13f ), GUILayout.Height(13f) ) )
					{
						layout.animations.Add( new SpritesheetAnimation() );
					}
				GUILayout.EndHorizontal();

				animationSelectorScroll = GUILayout.BeginScrollView( animationSelectorScroll );
					int deadAnimationIndex = -1;
					for( int i = 0; i < layout.animations.Count; ++i )
					{
						SpritesheetAnimation anim = layout.animations[i];
						bool isSelected = i == selectedAnimationIndex;
						GUIStyle style = isSelected ? listItemHighlight : listItemNormal;
						GUILayout.BeginHorizontal();
							if( GUILayout.Button( anim.name, style ) )
							{
								selectedAnimationIndex = i;
							}

							if( GUILayout.Button( EditorIcons.X, style, GUILayout.Width( 18f ), GUILayout.Height(18f) ) )
							{
								deadAnimationIndex = i;
							}
						GUILayout.EndHorizontal();
					}

					if( layout.animations.IsValidIndex( deadAnimationIndex ) )
					{
						layout.animations.RemoveAt( deadAnimationIndex );
					}
				GUILayout.EndScrollView();
			}

			if( GUILayout.Button( new GUIContent( "  New", EditorIcons.New ), buttonNormal, GUILayout.Height( 20f ) ) )
			{
				layout = new SpritesheetLayout();
			}

			GUILayout.BeginHorizontal();
				if( GUILayout.Button( new GUIContent( "  Load", EditorIcons.Load ), buttonNormal, GUILayout.Height( 20f ) ) )
				{
					layout = SpritesheetLayout.CreateWithDialog();
				}

				if( GUILayout.Button( new GUIContent( "  Save", EditorIcons.Save ), buttonNormal, GUILayout.Height( 20f ) ) )
				{
					if( layout != null )
						layout.SaveWithDialog();
				}
			GUILayout.EndHorizontal();
		}

		private void DoAnimationDetailsGUI( SpritesheetAnimation anim )
		{
			if( anim == null ) return;

			GUILayout.Label( "Animation Details:" );
			GUILayout.BeginHorizontal( GUILayout.Height( 50f ) );
				// preview //
				GUIContent animSpriteContent = new GUIContent( System.String.Empty );
				SpritesheetAnimationFrame previewFrame = anim.GetFrameAtTime( (float)(EditorApplication.timeSinceStartup % anim.duration), true );
				if( previewFrame != null )
				{
					Texture2D previewTex = layout.GetTextureAt( previewFrame.index );
					if( previewTex != null )
					{
						animSpriteContent = new GUIContent( previewTex );
					}
				}
				GUILayout.Label( animSpriteContent, buttonNormal, GUILayout.Height( 50f ), GUILayout.Width( 50f ) );

				// info
				GUILayout.BeginVertical();
					anim.name = EditorGUILayout.TextField( "Name", anim.name );
					GUILayout.Label( "Duration: " + anim.duration.ToString() );
				GUILayout.EndVertical();

				// add button
				GUILayout.BeginVertical( GUILayout.Width( 20f ) );
					GUILayout.FlexibleSpace();
					if( GUILayout.Button( EditorIcons.Plus, buttonNormal, GUILayout.Width( 13f ), GUILayout.Height(13f) ) )
					{
						anim.frames.Add( new SpritesheetAnimationFrame() );
					}
				GUILayout.EndVertical();
			GUILayout.EndHorizontal();

			GUILayout.Space( 10f );

			GUILayout.BeginVertical( GUI.skin.box );
				GUILayout.Label( "Frames:" );
				frameSelectorScroll = GUILayout.BeginScrollView( frameSelectorScroll );
					int deadIndex = -1;
					int moveUpIndex = -1;
	 				int moveDownIndex = -1;

					for( int i = 0; i < anim.frames.Count; ++i )
					{
						SpritesheetAnimationFrame frame = anim.frames[i];
						GUILayout.BeginHorizontal( GUI.skin.box );
							Texture2D preview = layout.GetTextureAt( frame.index );
							GUIContent previewContent = preview == null ? new GUIContent( frame.index == -1 ? "[NONE]" : frame.index.ToString() ) : new GUIContent( preview );

							bool isSelected = i == selectedFrameIndex;
							GUIStyle style = isSelected ? listItemHighlight : listItemNormal;

							if( GUILayout.Button( previewContent, style, GUILayout.Height(50f), GUILayout.Width(50f) ) )
							{
								selectedFrameIndex = i;
							}

							GUILayout.BeginVertical();
								GUILayout.BeginHorizontal();
									if( GUILayout.Button( EditorIcons.ArrowUp, buttonNoBorder, GUILayout.Width( 18f ), GUILayout.Height( 18f ) ) )
									{
										moveUpIndex = i;
									}
									if( GUILayout.Button( EditorIcons.ArrowDown, buttonNoBorder, GUILayout.Width( 18f ), GUILayout.Height( 18f ) ) )
									{
										moveDownIndex = i;
									}
									GUILayout.FlexibleSpace();
									if( GUILayout.Button( EditorIcons.X, buttonNoBorder, GUILayout.Width( 18f ), GUILayout.Height( 18f ) ) )
									{
										deadIndex = i;
									}
								GUILayout.EndHorizontal();
								frame.duration = EditorGUILayout.FloatField( "Duration (seconds)", frame.duration );
							GUILayout.EndVertical();
						GUILayout.EndHorizontal();
					}

					if( anim.frames.IsValidIndex( deadIndex ) )
					{
						anim.frames.RemoveAt( deadIndex );
					}
					else if( anim.frames.IsValidIndex( moveUpIndex ) )
					{
						anim.frames.SwapItems( moveUpIndex, moveUpIndex-1 );
					}
					else if( anim.frames.IsValidIndex( moveDownIndex ) )
					{
						anim.frames.SwapItems( moveDownIndex, moveDownIndex+1 );
					}
				GUILayout.EndScrollView();
				GUILayout.FlexibleSpace();
			GUILayout.EndVertical();
		}

		private void DoSpriteSelectorGUI()
		{
			layout.spritesheet = btGui.ThinObjectField<Texture2D>( "Spritesheet", layout.spritesheet, false );
			if( layout.spritesheet != null )
			{
				spriteSelectorScroll = GUILayout.BeginScrollView( spriteSelectorScroll, GUILayout.Height( 75f ) );
					GUILayout.BeginHorizontal();
						for( int i = 0; i < layout.spriteTextures.Length; ++i )
						{
							if( GUILayout.Button( layout.spriteTextures[i], listItemNormal, GUILayout.Width( 50f ), GUILayout.Height( 50f ) ) )
							{
								SpritesheetAnimationFrame frame = selectedFrame;
								if( frame != null )
								{
									frame.index = i;
								}
							}
						}
					GUILayout.EndHorizontal();
				GUILayout.EndScrollView();
			}
		}
	}
}
