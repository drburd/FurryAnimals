//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
	public static partial class btGui
	{
        //
        // object reflection gui ###############################################
        //
		public static object ObjectReflectionGUI( object obj )
		{
			return ObjectReflectionGUI( System.String.Empty, obj );
		}

		public static object ObjectReflectionGUI( string label, object obj )
		{
			return ObjectReflectionGUI( label, obj, null, null );
		}

		public static object ObjectReflectionGUI( string label, object obj, object DEBUG_parent, System.Type _type )
		{
			if( obj == null && _type == null )
			{
				EditorGUILayout.LabelField( label, "[null]" );
				return obj;
			}

			System.Type type = _type ?? obj.GetType();
			if( type == typeof( Bounds ) )
			{
				obj = (object)EditorGUILayout.BoundsField( label, (Bounds)obj );
			}
			else if( type == typeof( Color ) )
			{
				obj = (object)EditorGUILayout.ColorField( label, (Color)obj );
			}
			else if( type == typeof( AnimationCurve ) )
			{
				obj = (object)EditorGUILayout.CurveField( label, (AnimationCurve)obj );
			}
			else if( type.IsEnum )
			{
				obj = (object)EditorGUILayout.EnumPopup( label, (System.Enum)obj );
			}
			else if( type == typeof( float ) || type == typeof( double ) || type == typeof( decimal ) )
			{
				obj = (object)EditorGUILayout.FloatField( label, (float)System.Convert.ToSingle(obj) );
			}
			else if( type == typeof( int ) )
			{
				obj = (object)EditorGUILayout.IntField( label, (int)obj );
			}
			else if( typeof( UnityEngine.Object ).IsAssignableFrom( type ) )
			{
				obj = (object)EditorGUILayout.ObjectField( label, (UnityEngine.Object)obj, typeof( UnityEngine.Object ), true );
			}
			else if( type == typeof( Rect ) )
			{
				obj = (object)EditorGUILayout.RectField( label, (Rect)obj );
			}
			else if( type == typeof( System.String ) )
			{
				obj = (object)EditorGUILayout.TextField( label, (string)obj );
			}
			else if( type == typeof( bool ) )
			{
				obj = (object)EditorGUILayout.Toggle( label, (bool)obj );
			}
			else if( type == typeof( Vector2 ) )
			{
				obj = (object)EditorGUILayout.Vector2Field( label, (Vector2)obj );
			}
			else if( type == typeof( Vector3 ) )
			{
				obj = (object)EditorGUILayout.Vector3Field( label, (Vector3)obj );
			}
			else if( type == typeof( Vector4 ) )
			{
				obj = (object)EditorGUILayout.Vector4Field( label, (Vector4)obj );
			}
			else if( type.IsArray )
			{
				EditorGUILayout.LabelField( System.String.Empty, label );

				EditorGUI.indentLevel += 1;

				System.Array arr = (System.Array)obj;
				if( arr != null )
				{
					for( int i = 0; i < arr.Length; ++i )
					{
						object item = arr.GetValue( i );
						ObjectReflectionGUI( "Item" + i.ToString(), item, obj, item.GetType() );
						arr.SetValue( item, i );
					}

					EditorGUI.indentLevel -= 1;
				}

				obj = (object)arr;
			}
			else
			{
				EditorGUILayout.LabelField( System.String.Empty, label );

				EditorGUI.indentLevel += 1;

				FieldInfo[] fields = type.GetFields();
				foreach( FieldInfo field in fields )
				{
					bool isPublic = field.IsPublic;
					bool isSerializeField = field.GetCustomAttributes( typeof(UnityEngine.SerializeField), true ).Length > 0;
					bool isNonSerialized = field.GetCustomAttributes( typeof(System.NonSerializedAttribute), true ).Length > 0;
					bool isStatic = field.IsStatic;
					if( isStatic == false && isNonSerialized == false && ( isSerializeField || isPublic ) )
					{
						// when using Component references on prefabs, it passes the game object around,
						// so we need to manually GetComponent to get the type we want out of it.
						object original = field.GetValue( obj );
						object result = ObjectReflectionGUI( field.Name, original, obj, field.FieldType );
						if( result != original )
						{
							System.Type resultType = result.GetType();
							System.Type desiredType = field.FieldType;
							if( resultType != desiredType && resultType == typeof(GameObject) )
							{
								GameObject go = (GameObject)result;
								result = go.GetComponent( desiredType );
							}
						}

						field.SetValue( obj, result );
					}
				}

				EditorGUI.indentLevel -=1;
			}

			return obj;
		}
	}
}
