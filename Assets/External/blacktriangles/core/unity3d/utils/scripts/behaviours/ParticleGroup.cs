//==============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//==============================================================================

﻿using UnityEngine;
using System.Collections;

namespace blacktriangles
{
	public class ParticleGroup
		: MonoBehaviour
	{
		// members /////////////////////////////////////////////////////////////
		[SerializeField] ParticleSystem[] systems           = null;
        [SerializeField] bool useChildrenSystems            = true;

        // public methods //////////////////////////////////////////////////////
        public void Play()
        {
            foreach( ParticleSystem system in systems )
            {
                system.Play();
            }
        }

        public void Stop()
        {
            foreach( ParticleSystem system in systems )
            {
                system.Stop();
            }
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            if( useChildrenSystems )
            {
                systems = transform.GetComponentsInChildren<ParticleSystem>( false );
            }
        }
	}
}
