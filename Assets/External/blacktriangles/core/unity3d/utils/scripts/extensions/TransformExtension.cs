//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections.Generic;

public static class TransformExtension
{
	// public methods /////////////////////////////////////////////////////////
	public static Transform[] GetAllChildren( this Transform root )
	{
		if( root == null ) return new Transform[0];

		List<Transform> children = new List<Transform>();
		GetAllChildren( root, children );
		return children.ToArray();
	}

	public static Transform FindRecursive( this Transform root, string name )
	{
		if( root == null ) return null;

		foreach( Transform child in root )
		{
			if( child.name == name ) return child;
			else return FindRecursive( child, name );
		}

		return null;
	}

	// private methods ////////////////////////////////////////////////////////
	private static void GetAllChildren( Transform root, List<Transform> children )
	{
		foreach( Transform child in root.transform )
		{
			children.Add( child );
			GetAllChildren( child, children );
		}
	}

}
