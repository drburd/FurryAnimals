//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

Shader "blacktriangles/FlatStylized" {
    Properties {
		_Side("Side", 2D) = "white" {}
        _SideColor("SideColor", Color) = (1,1,1,1)
		_Top("Top", 2D) = "white" {}
        _TopColor("TopColor", Color) = (1,1,1,1)
		_Bottom("Bottom", 2D) = "white" {}
        _BottomColor("BottomColor", Color) = (1,1,1,1)
		_SideScale("Side Scale", Float) = 2
		_TopScale("Top Scale", Float) = 2
		_BottomScale ("Bottom Scale", Float) = 2
        _Banding("Banding", Range(0,100)) = 100
    }

    SubShader {
        Tags {
            "RenderType"="Opaque"
        }

        //
        // FORWARD BASE ########################################################
        //
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }

            CGPROGRAM
                // compiler settings ///////////////////////////////////////////
                #pragma vertex vert
                #pragma fragment frag
                #define UNITY_PASS_FORWARDBASE
                #include "UnityCG.cginc"
                #include "AutoLight.cginc"
                #include "Lighting.cginc"
                #include "Triplanar.cginc"
                #include "btLightUtil.cginc"
                #pragma multi_compile_fwdbase_fullshadows
                #pragma multi_compile_fog
                #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2
                #pragma target 3.0

                // inputs / outputs ////////////////////////////////////////////
                struct VertexInput {
                    float4 vertex : POSITION;
                    float3 normal : NORMAL;
                    float2 texcoord0 : TEXCOORD0;
                };

                struct VertexOutput {
                    float4 pos : SV_POSITION;
                    float2 uv0 : TEXCOORD0;
                    float4 posWorld : TEXCOORD1;
                    float3 normalDir : TEXCOORD2;
                    LIGHTING_COORDS(3,4)
                    UNITY_FOG_COORDS(5)
                };

                // vertex shader ///////////////////////////////////////////////
                VertexOutput vert (VertexInput v) {
                    VertexOutput o = (VertexOutput)0;
                    o.uv0 = v.texcoord0;
                    o.normalDir = normalize(UnityObjectToWorldNormal(v.normal));
                    o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                    o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                    UNITY_TRANSFER_FOG(o,o.pos);
                    TRANSFER_VERTEX_TO_FRAGMENT(o)
                    return o;
                }

                // fragment shader /////////////////////////////////////////////
                float4 frag(VertexOutput i) : COLOR {

                    float lightPower = calculateLightPower( LIGHT_ATTENUATION(i), i.posWorld, i.normalDir );
                    float3 pixelColor = triplanarPixelColor(i.normalDir, i.posWorld);
                    //pixelColor = colorizeBasedOnLightPower(lightPower);

                    // lighting
                    float3 emissive = (pixelColor*UNITY_LIGHTMODEL_AMBIENT.rgb);
                    float3 finalColor = emissive + pixelColor*lightPower*_LightColor0.rgb;
                    fixed4 finalRGBA = fixed4(finalColor,1);
                    UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                    return finalRGBA;
                }
            ENDCG
        }

        //
        // FORWARD ADD #########################################################
        //
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One

            CGPROGRAM
            // compiler settings ///////////////////////////////////////////////
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "Triplanar.cginc"
            #include "btLightUtil.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2
            #pragma target 3.0

            // inputs / outputs ////////////////////////////////////////////////
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };

            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };

            // vertex shader ///////////////////////////////////////////////////
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }

            // fragment shader /////////////////////////////////////////////////
            float4 frag(VertexOutput i) : COLOR {
                float lightPower = calculateLightPower( LIGHT_ATTENUATION(i), i.posWorld, i.normalDir );
                float3 pixelColor = triplanarPixelColor(i.normalDir, i.posWorld);
                //pixelColor = colorizeBasedOnLightPower(lightPower);

                ////// Lighting:
                float3 lightColor = _LightColor0.rgb;
                float3 finalColor = pixelColor*lightPower*_LightColor0.rgb;
                fixed4 finalRGBA = fixed4(finalColor,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
