//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using blacktriangles;

namespace blacktriangles.Input
{
	public class InputRouter<ActionTypeEnum>
		: IJsonSerializable
	{
		// members ////////////////////////////////////////////////////////////////
		public System.Type actionType							{ get { return typeof(ActionTypeEnum); } }
		private Dictionary<ActionTypeEnum,InputState> states    = new Dictionary<ActionTypeEnum,InputState>();

    	public Vector2 mousePos                                 { get { return UnityEngine.Input.mousePosition; } }

		// public methods /////////////////////////////////////////////////////////
        public void Update()
        {
            foreach( InputState state in states.Values )
            {
                state.Update();
            }
        }

        public void Bind( ActionTypeEnum type, InputAction action )
        {
            InputState state = null;
            if( states.TryGetValue( type, out state ) == false )
            {
                state = new InputState();
            }

            state.actions.Add( action );
            states[type] = state;
        }

        public void Unbind( ActionTypeEnum type )
        {
            states.Remove( type );
        }

        public float GetAxis( ActionTypeEnum type )
        {
            InputState state = this[type];
            return state.GetAxis();
        }

		public bool GetKeyDown( ActionTypeEnum type )
        {
            InputState state = this[type];
            return state.GetKeyDown();
        }

		public bool GetKey( ActionTypeEnum type )
        {
            InputState state = this[type];
            return state.GetKey();
        }

		public bool GetKeyUp( ActionTypeEnum type )
        {
            InputState state = this[type];
            return state.GetKeyUp();
        }

        public float GetKeyDownElapsed( ActionTypeEnum type )
        {
            InputState state = this[type];
            return state.wasKeyPressed.elapsed;
        }

        public float GetKeyElapsed( ActionTypeEnum type )
        {
            InputState state = this[type];
            return state.isKeyDown.elapsed;
        }

        public float GetKeyUpElapsed( ActionTypeEnum type )
        {
            InputState state = this[type];
            return state.wasKeyReleased.elapsed;
        }

        // operators //////////////////////////////////////////////////////////
        public InputState this[ System.Enum type ]
        {
            get
            {
                ActionTypeEnum action = ConvertActionType( type );
                return this[action];
            }
        }

        public InputState this[ ActionTypeEnum type ]
        {
            get
            {
                InputState result = null;
                states.TryGetValue( type, out result );
                return result;
            }
        }

        // private methods ////////////////////////////////////////////////////
        private ActionTypeEnum ConvertActionType( System.Enum type )
        {
            return (ActionTypeEnum)((object)type);
        }

		// IJsonSerializable //////////////////////////////////////////////////
		public JsonObject ToJson()
		{
			throw new System.NotImplementedException();
		}

		public void FromJson( JsonObject json )
		{
			throw new System.NotImplementedException();
		}
	}
}
