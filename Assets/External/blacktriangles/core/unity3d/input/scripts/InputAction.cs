//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace blacktriangles.Input
{
	public class InputAction
		: IJsonSerializable
	{
		// types //////////////////////////////////////////////////////////////////
		public enum Axis
		{
			None
			, MouseHorizontal
			, MouseVertical
			, MouseWheel
			, JoystickAxis1
			, JoystickAxis2
			, JoystickAxis3
			, JoystickAxis4
			, JoystickAxis5
			, JoystickAxis6
			, JoystickAxis7
			, JoystickAxis8
			, JoystickAxis9
			, JoystickAxis10
		};

		private static readonly string[] kAxisToString =
		{
			"None"
			, "Mouse Horizontal"
			, "Mouse Vertical"
			, "Mouse Wheel"
			, "Joy1 Axis 1"
			, "Joy1 Axis 2"
			, "Joy1 Axis 3"
			, "Joy1 Axis 4"
			, "Joy1 Axis 5"
			, "Joy1 Axis 6"
			, "Joy1 Axis 7"
			, "Joy1 Axis 8"
			, "Joy1 Axis 9"
			, "Joy1 Axis 10"
		};

		public enum Type { Key, Axis, Combo };

		// members ////////////////////////////////////////////////////////////
		public KeyCode keycode									= KeyCode.None;
		public KeyCode negativeCode							    = KeyCode.None;
		public KeyCode comboKey									= KeyCode.None;
		public Axis axis										= Axis.None;
		public string axisName									{ get { return kAxisToString[(int)axis]; } }
		public bool invertAxis									= false;
        public bool deferToUI                                   = false;
		public float sensitivity								= 1f;

		public Type type										{ get; private set; }
        public bool shouldDefer                                 { get { return deferToUI && EventSystem.current.IsPointerOverGameObject(); } }

		// constructor / initializer //////////////////////////////////////////
		public static InputAction FromKey( KeyCode code, bool defer = false )
		{
			InputAction result = new InputAction();
			result.type = Type.Key;
			result.keycode = code;
            result.deferToUI = defer;
			return result;
		}

		public static InputAction FromAxis( Axis axis, bool invert, float sensitivity, bool defer = false )
		{
			InputAction result = new InputAction();
			result.type = Type.Axis;
			result.keycode = KeyCode.None;
			result.axis = axis;
			result.invertAxis = invert;
			result.sensitivity = sensitivity;
            result.deferToUI = defer;
			return result;
		}

		public static InputAction FromAxis( KeyCode minus, KeyCode plus, bool defer = false )
		{
			InputAction result = new InputAction();
			result.type = Type.Axis;
			result.keycode = plus;
			result.negativeCode = minus;
            result.deferToUI = defer;
			return result;
		}

		public static InputAction FromCombo( KeyCode key1, KeyCode key2, bool defer = false )
		{
			InputAction result = new InputAction();
			result.type = Type.Combo;
			result.keycode = key1;
			result.comboKey = key2;
            result.deferToUI = defer;
			return result;
		}

		private InputAction()
		{
		}

        // public methods /////////////////////////////////////////////////////
        public float GetAxis()
		{
			float result = 0f;
            if( shouldDefer ) return result;

			if( type == InputAction.Type.Axis )
			{
				if( axis != InputAction.Axis.None )
				{
					result = UnityEngine.Input.GetAxis( axisName ) * ( invertAxis ? -1f : 1f ) * sensitivity;
				}
				else if( negativeCode != KeyCode.None )
				{
					float positive = UnityEngine.Input.GetKey( keycode ) ? 1f : 0f;
					float negative = UnityEngine.Input.GetKey( negativeCode ) ? 1f : 0f;
					result = positive - negative;
				}
			}

			return result;
		}

		public bool GetKeyDown()
		{
			bool result = false;
            if( shouldDefer ) return result;

			if( type == InputAction.Type.Key )
			{
				result = result || UnityEngine.Input.GetKeyDown( keycode );
			}
			else if( type == InputAction.Type.Combo )
			{
				// either key was pressed this frame, and the other key is being pressed.
				bool didComboCompleteThisFrame = ( UnityEngine.Input.GetKeyDown( keycode ) && UnityEngine.Input.GetKey( comboKey ) )
												|| ( UnityEngine.Input.GetKey( keycode ) && UnityEngine.Input.GetKeyDown( comboKey ) );

				result = result || didComboCompleteThisFrame;
			}

			return result;
		}

		public bool GetKey()
		{
			bool result = false;
            if( shouldDefer ) return result;

			if( type == InputAction.Type.Key )
			{
				result = result || UnityEngine.Input.GetKey( keycode );
			}
			else if( type == InputAction.Type.Combo )
			{
				result = result || (UnityEngine.Input.GetKey( keycode ) && UnityEngine.Input.GetKey( comboKey ));
			}

			return result;
		}

		public bool GetKeyUp()
		{
			bool result = false;
            if( shouldDefer ) return result;

			if( type == InputAction.Type.Key )
			{
				result = result || UnityEngine.Input.GetKeyUp( keycode );
			}

			return result;
		}

		// IJsonSerializable //////////////////////////////////////////////////
		public JsonObject ToJson()
		{
			JsonObject result = new JsonObject();
			result["keycode"] = keycode;
			result["negativeCode"] = negativeCode;
			result["axis"] = axis;
			result["invertAxis"] = invertAxis;
			result["sensitivity"] = sensitivity;
			return result;
		}

		public void FromJson( JsonObject json )
		{
			keycode = json.GetField<KeyCode>("keycode");
			negativeCode = json.GetField<KeyCode>("negativeCode");
			axis = json.GetField<Axis>("axis");
			invertAxis = json.GetField<bool>("invertAxis");
			sensitivity = json.GetField<float>("sensitivity");
		}
	};
}
