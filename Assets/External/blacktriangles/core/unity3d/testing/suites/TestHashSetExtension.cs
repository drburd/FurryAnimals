//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using blacktriangles;
using blacktriangles.Test;
using System.Collections.Generic;

namespace blacktriangles.Testing
{
    [TestSuite("TestHashSetExtension")]
    public class TestHashSetExtension
        : TestSuite
    {
        // members /////////////////////////////////////////////////////////////

        // public methods //////////////////////////////////////////////////////
        public override void RegisterTests()
        {
            AddTest( "TestHashSetExtension", BasicTest );
        }

        // private methods /////////////////////////////////////////////////////
		private void BasicTest()
		{
            HashSet<int> intSet = new HashSet<int>();
            Assert( intSet.ToArray().Length == 0 );

            intSet.Add( 1 );
            Assert( intSet.ToArray().Length == 1 );

            intSet.Add( 100 );
            Assert( intSet.ToArray().Length == 2 );

            intSet.Add( 50 );
            int[] array = intSet.ToArray();
            Assert( array.Length == 3 );
            Assert( array[0] == 1 );
            Assert( array[1] == 100 );
            Assert( array[2] == 50 );
		}
    }
}

#endif
