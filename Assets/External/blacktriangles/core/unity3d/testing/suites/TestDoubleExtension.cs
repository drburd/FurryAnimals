//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using blacktriangles;
using blacktriangles.Test;

namespace blacktriangles.Testing
{
	[TestSuite("TestDoubleExtension")]
	public class TestDoubleExtension
		: TestSuite
	{
		// members ////////////////////////////////////////////////////////////////

		// public methods /////////////////////////////////////////////////////////
		public override void RegisterTests()
		{
			AddTest( "TestDoubleExtension", BasicTest );
		}

		// private methods ////////////////////////////////////////////////////////
		private void BasicTest()
		{
			double d = 0;
			Assert( d.IsNearZero() );
			Assert( d.IsApproximately( 10 ) == false );

			d = 10;
			Assert( d.IsNearZero() == false );
			Assert( d.IsApproximately( 10 ) );
			Assert( d.IsApproximately( 100 / 10 ) );
		}
	}
}

#endif
