//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using blacktriangles;
using blacktriangles.Test;

namespace blacktriangles.Testing
{
    [TestSuite("TestFloatExtension")]
    public class TestFloatExtension
        : TestSuite
    {
        // members ////////////////////////////////////////////////////////////////

        // public methods /////////////////////////////////////////////////////////
        public override void RegisterTests()
        {
            AddTest( "TestFloatExtension", BasicTest );
        }

        // private methods ////////////////////////////////////////////////////////
        private void BasicTest()
        {
            float f = 0f;
            Assert( f.IsNearZero() );
            Assert( f.IsApproximately( 10f ) == false );

            f = 10f;
            Assert( f.IsNearZero() == false );
            Assert( f.IsApproximately( 10f ) );
            Assert( f.IsApproximately( 100f / 10f ) );
            Assert( f.IsApproximately( 100f ) == false );
        }
    }
}

#endif
