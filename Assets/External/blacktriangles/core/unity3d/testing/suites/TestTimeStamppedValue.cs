//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using blacktriangles;
using blacktriangles.Test;
using System.Collections;

namespace blacktriangles.Testing
{
    [TestSuite("TestTimeStamppedValue")]
    public class TestTimeStamppedValue
        : TestSuite
    {
        // members ////////////////////////////////////////////////////////////////

        // public methods /////////////////////////////////////////////////////////
        public override void RegisterTests()
        {
          AddTest( "TestTimeStamppedValue", BasicTest );
        }

        // private methods ////////////////////////////////////////////////////////
        private void BasicTest()
        {
            TimestampedValue<int> stampedInt = new TimestampedValue<int>();

            // newly created timestamped values have their timestamp and access stamp
            // set to the time of creation so they will be equal.
            double timestamp = stampedInt.timestamp;
            double accessTimestamp = stampedInt.accessTimestamp;
            Assert( accessTimestamp.IsApproximately( timestamp ) );

            // you can test for equivalency in both directions both against the
            // internal .val or the timestamped value itself through use of
            // implicit casting.
            Assert( stampedInt.val == 0 );
            Assert( stampedInt == 0 );
            Assert( 0 == stampedInt );

            // similarly you can set the .val or stamped value directly.
            stampedInt = 10;
            Assert( stampedInt.val == 10 );
            Assert( stampedInt == 10 );
            Assert( 10 == stampedInt );
        }
    }
}

#endif
