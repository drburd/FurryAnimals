//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using blacktriangles;
using blacktriangles.Test;
using System.Collections;
using System.Collections.Generic;

namespace blacktriangles.Testing
{
    [TestSuite("TestArgumentParser")]
    public class TestArgumentParser
        : TestSuite
    {
        // members ////////////////////////////////////////////////////////////////

        // public methods /////////////////////////////////////////////////////////
        public override void RegisterTests()
        {
            AddTest( "ArgumentParse", BasicTest );
            AddTest( "DynamicArgumentParse", DynamicArgumentParseTest );
        }

        // private methods ////////////////////////////////////////////////////////
        private void BasicTest()
        {
            ArgumentParser parser = new ArgumentParser( new string[] { "app", "-test", "value1", "value2", "--test2", "value3", "-test3", "-test", "value4", "value5", "value6" } );

            // we only parse arguments taht start with a dash
            Assert( parser.HasArgument( "app" ) == false );

            // arguments have leading dashes stripped
            Assert( parser.HasArgument( "test" ) );

            // double dash is acceptable
            Assert( parser.HasArgument( "test2" ) );
            Assert( parser.HasArgument( "test3" ) );

            // this was never added
            Assert( parser.HasArgument( "test4" ) == false );

            Assert( parser.GetValues( "test" ) != null );
            Assert( parser.GetValues( "test" ).Count == 3 );
            Assert( parser.GetValues( "test" )[0] == "value4" );
            Assert( parser.GetValues( "test" )[1] == "value5" );
            Assert( parser.GetValues( "test" )[2] == "value6" );

            Assert( parser.GetValues( "test2" ).Count == 1 );
            Assert( parser.GetValues( "test2" )[0] == "value3" );

            Assert( parser.GetValues( "test3" ).Count == 0 );
        }

        private void DynamicArgumentParseTest()
        {
            ArgumentParser parser = new ArgumentParser( new string[] {
                    "app",
                    "-test", "hello",
                    "-test2", "1.34", "4.31",
                    "-test3", "1337",
                    "-test4", "true",
                    "-test5", "TRUE",
                    "-test6", "false",
                    "-test7", "FALSE",
                });

            Assert( parser.GetValue<string>( "test", "FALSE" ) == "hello" );
            Assert( parser.GetValue<float>( "test2", 500.0f ).IsApproximately( 1.34f ) );
            Assert( parser.GetValue<double>( "test2", 500.0 ).IsApproximately( 1.34 ) );
            Assert( parser.GetValue<int>( "test3", 1337 ) == 1337 );
            Assert( parser.GetValue<bool>( "test4", false ) == true );
            Assert( parser.GetValue<bool>( "test5", false ) == true );
            Assert( parser.GetValue<bool>( "test6", true ) == false );
            Assert( parser.GetValue<bool>( "test7", true ) == false );

            //invalid types throw an exception
            bool success = false;
            try
            {
                parser.GetValue<List<string>>( "test12", null );
            }
            catch( System.ArgumentException )
            {
                success = true;
            }
            Assert( success );

        }
    }
}

#endif
