//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using UnityEngine;
using ProtoBuf;

namespace blacktriangles.Network
{
    [ProtoContract]
    public class pbVector3
    {
        [ProtoMember(1)] float x;
        [ProtoMember(2)] float y;
        [ProtoMember(3)] float z;

        // operators ///////////////////////////////////////////////////////////
        public static implicit operator pbVector3( Vector3 vector3 )
        {
            pbVector3 result = new pbVector3();
            result.x = vector3.x;
            result.y = vector3.y;
            result.z = vector3.z;
            return result;
        }

        public static implicit operator pbVector3( Quaternion quaternion )
        {
            pbVector3 result = quaternion.eulerAngles;
            return result;
        }

        public static implicit operator Vector3( pbVector3 pbvec3 )
        {
            return new Vector3( pbvec3.x, pbvec3.y, pbvec3.z );
        }

        public static implicit operator Quaternion( pbVector3 pbvec3 )
        {
            return Quaternion.Euler( pbvec3.x, pbvec3.y, pbvec3.z );
        }
    }
}
