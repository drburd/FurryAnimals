//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;

namespace blacktriangles.Network
{
    [System.AttributeUsage( System.AttributeTargets.Class )]
	public class JsonActionAttribute
        : System.Attribute
	{
        // members ////////////////////////////////////////////////////////////
        public string name									{ get; private set; }

        // constructor / destructor ///////////////////////////////////////////
        public JsonActionAttribute( string _name )
        {
			name = _name;
        }
    }
}
