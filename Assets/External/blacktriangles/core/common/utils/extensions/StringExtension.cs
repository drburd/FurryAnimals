//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;

public static class StringExtension
{
	public static bool Contains( this string self, char c )
	{
		return self.IndexOf( c ) != -1;
	}

    public static bool ContainsIgnoreCase( this string self, string str )
    {
        return self.IndexOf( str, System.StringComparison.OrdinalIgnoreCase ) != -1;
    }

	public static string[] SplitByLine( this string self )
	{
		return self.Split( '\n' );
	}

    public static string MaxLength( this string self, int length, bool addElipsis = true )
    {
        if( self.Length < length ) return self;
        string substr = self.Substring( 0, System.Math.Min( length, self.Length ) );
        if( addElipsis ) substr += "...";
        return substr;
    }
}
