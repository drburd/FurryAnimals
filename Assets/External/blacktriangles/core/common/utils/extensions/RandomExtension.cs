//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;

public static class RandomExtension
{
	public static double NextDouble( this System.Random self, double min, double max )
	{
		double rng = self.NextDouble();
		return rng * (min + (max-min));
	}

	public static float NextFloat( this System.Random self )
	{
		return (float)(self.NextDouble() % System.Single.MaxValue);
	}

	public static float NextFloat( this System.Random self, float min, float max )
	{
		float rng = self.NextFloat();
		return rng * (min + (max-min));
	}
}
