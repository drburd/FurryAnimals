﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat : MonoBehaviour {

    public bool isAttackSuccessful = false;

    public Transform currentTarget;
    public CombatDamageText combatDamageTextScript;

    // this should be the selected unit attack damage
    public int tempDamage;
    public string dodgedAttack;

    private UnitCommands unitCommands;
    private UnitStats targetStats;
    private int targetHP;
    private int targetDodge;
    private int dodgeCheck;
    
    public void BeginAttack()
    {
        dodgeCheck = Random.Range(0, 100);
    // identify target and get some of its stats
        unitCommands = GetComponent<UnitCommands>();
        currentTarget = unitCommands.whichTarget;
        targetStats = currentTarget.GetComponent<UnitStats>();
        targetHP = targetStats.hitPoints;
        targetDodge = targetStats.dodgeChance;
        // tempDamage = 100;

        // Find selected objects
        GameObject[] gameObjects = FurryUtil.getFriendlyUnits ();
		foreach (GameObject gameObject in gameObjects)
		{
            Debug.Log(gameObject.name);

			Selectable selectableGameObject = gameObject.GetComponent<Selectable> ();
			if (selectableGameObject == null)
            {
				selectableGameObject = gameObject.GetComponentInChildren<Selectable> ();
		    }
			if (selectableGameObject == null)
            {
				selectableGameObject = gameObject.GetComponentInParent<Selectable> ();
    		}
// end Find selected objects

            

            if (!selectableGameObject.IsSelected())
			{
				continue;
			}

            // find current unit's attack damage
            tempDamage = gameObject.GetComponent<UnitStats>().meleeDamage;

            // check to see if the target dodged the attack
            if (targetDodge < dodgeCheck)
			{
				isAttackSuccessful = true;

                // Apply damage to target           
                targetHP = targetHP - tempDamage;   // replace tempDamage with the unit's attack damage variable
                targetStats.hitPoints = targetHP;
            }
       
            combatDamageTextScript = currentTarget.GetComponent<CombatDamageText>();
            
            if (combatDamageTextScript == null)
            {
                   return;
    	    }
            
            // if dodged
            if(isAttackSuccessful != true)
            {
                dodgedAttack = "Dodge!";
                combatDamageTextScript.initCombatDamageText(dodgedAttack.ToString()); // pass "Dodge!" string to the initCombatDamageText function
            }
            
            // if not dodged, target takes damage.
            if (isAttackSuccessful == true)
            {
                combatDamageTextScript.initCombatDamageText(tempDamage.ToString()); // pass damage integer to the initCombatDamageText function as a string

                Debug.Log("Target HP is now  " + targetHP);
            }
        }

			


			isAttackSuccessful = false;
    }

	void Update()
	{
	}
}

