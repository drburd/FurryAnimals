﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCommands : MonoBehaviour {

    public Ray rightClickTarget;
    public RaycastHit rightClickHit;
    public string whatRayHit;
    public Combat combatScript;
    public Transform whichTarget;

	void Start ()
    {
		
	}

    public void MoveCommand()
    {
		GameObject[] gameObjects = FurryUtil.getFriendlyUnits ();
        foreach (GameObject gameObject in gameObjects)
        {
			Selectable selectableGameObject = gameObject.GetComponent<Selectable> ();
			if (selectableGameObject == null) {
				selectableGameObject = gameObject.GetComponentInChildren<Selectable> ();
			}

			if (selectableGameObject == null) {
				selectableGameObject = gameObject.GetComponentInParent<Selectable> ();
			}

			if (!selectableGameObject.IsSelected())
            {
                continue;
            }

            Move moveScript = gameObject.GetComponent<Move>();
			if (moveScript == null) {
				moveScript = gameObject.GetComponentInChildren<Move> ();
			}

			if (moveScript == null) {
				moveScript = gameObject.GetComponentInParent<Move> ();
			}

            moveScript.MoveUnit(rightClickHit.point);
        }
    }

    public void LookCommand()
    {
        Debug.Log("This is a Look command.");
    }

        
    void Update ()
    {

        // Mouse position at time of click
        rightClickTarget = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Multi-function click
        //       if (Input.GetAxis("RightClick") > 0)
        if (Input.GetMouseButtonDown(1))
        {
                         
            // determine land, object, or enemy unit
                if (Physics.Raycast(rightClickTarget, out rightClickHit))
                {

                    whatRayHit = rightClickHit.collider.transform.parent.name;
                    whichTarget = rightClickHit.transform;    

                    Debug.Log(whatRayHit);
                
                     // if terrain, goto MoveCommand
                        if (whatRayHit == "Terrain")
                        {
							MoveCommand();
                        }

                       // if object, goto LookCommand

                        if (whatRayHit == "Environment")
                        {
                           LookCommand();
                        }
            
                       // if enemy unit, goto AttackCommand
                        if (whatRayHit == "Enemy")
                        {
                            combatScript = GetComponent<Combat>();
                            combatScript.BeginAttack();
                        }
            
            }

        }
}


}


			