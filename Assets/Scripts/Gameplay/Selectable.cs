﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selectable : MonoBehaviour {


	private bool isSelected = false;

	public bool IsSelected(){
		return isSelected;
	}

	public void Deselect(){
		isSelected = false;
		Projector unitSelectionProjector = GetComponentInChildren<Projector> ();
		unitSelectionProjector.enabled = false;
	}

	public void Select(){
		isSelected = true;
		Projector unitSelectionProjector = GetComponentInChildren<Projector> ();
		unitSelectionProjector.enabled = true;
	}
		
}
