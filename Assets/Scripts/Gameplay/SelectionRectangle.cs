﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionRectangle : MonoBehaviour {

    public bool isSelecting = false;
    public Vector3 mousePositionStart;
	public Rect selectionRectangle;


    void Start ()
    {
	}


// Functions for drawing rectangle
    static Texture2D _whiteTexture;
    public static Texture2D WhiteTexture
    {
        get
        {
            if (_whiteTexture == null)
            {
                _whiteTexture = new Texture2D(1, 1);
                _whiteTexture.SetPixel(0, 0, Color.white);
                _whiteTexture.Apply();
            }

            return _whiteTexture;
        }
    }

    public static Rect LocateSelection(Vector3 screenPosition1, Vector3 screenPosition2)
    {
        screenPosition1.y = Screen.height - screenPosition1.y;
        screenPosition2.y = Screen.height - screenPosition2.y;

        var topLeft = Vector3.Min(screenPosition1, screenPosition2);
        var bottomRight = Vector3.Max(screenPosition1, screenPosition2);

        return Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }


    public static void DrawSelection(Rect rect, Color color)
    {
        GUI.color = color;
        GUI.DrawTexture(rect, WhiteTexture);
        GUI.color = Color.white;
    }






// Call the rectangle functions and draw it
    void OnGUI()
    {
        if (isSelecting == true)
        {
            var selectionRectangle = LocateSelection(mousePositionStart, Input.mousePosition);
			this.selectionRectangle = selectionRectangle;
            DrawSelection(selectionRectangle, new Color(0.8f, 0.8f, 0.95f, 0.25f));
        }
    }


    void Update ()
    {
		if(Input.GetMouseButtonDown(0))
        {
            isSelecting = true;
            mousePositionStart = Input.mousePosition;            
        }

        if(Input.GetMouseButtonUp(0))
        {
            isSelecting = false;
			GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("friendly");
			foreach(GameObject gameObject in gameObjects){
				Selectable selectableGameObject = gameObject.GetComponent<Selectable> ();
				if (selectableGameObject == null) {
					selectableGameObject = gameObject.GetComponentInChildren<Selectable> ();
				}

				if (selectableGameObject == null) {
					selectableGameObject = gameObject.GetComponentInParent<Selectable> ();
				}
				if (IsWithinSelectionBounds (gameObject, this) && !selectableGameObject.IsSelected()) {
					selectableGameObject.Select ();
					continue;
				}

				if (!IsWithinSelectionBounds (gameObject, this) && selectableGameObject.IsSelected()) {
					selectableGameObject.Deselect ();
				}
			}

        }
    }

	// wonky WorldSpace stuff --- no idea.
	public static Bounds GetViewportBounds(Camera camera, Vector3 screenPosition1, Vector3 screenPosition2)
	{
		var v1 = Camera.main.ScreenToViewportPoint(screenPosition1);
		var v2 = Camera.main.ScreenToViewportPoint(screenPosition2);
		var min = Vector3.Min(v1, v2);
		var max = Vector3.Max(v1, v2);
		min.z = camera.nearClipPlane;
		max.z = camera.farClipPlane;

		var bounds = new Bounds();
		bounds.SetMinMax(min, max);
		return bounds;
	}

	// checks for GameObjects within the selection rectangle
	public bool IsWithinSelectionBounds(GameObject gameObject, SelectionRectangle selectionRectangle)
	{
		var camera = Camera.main;
		var viewportBounds = GetViewportBounds(camera, selectionRectangle.mousePositionStart, Input.mousePosition);

		return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));

	}
}
