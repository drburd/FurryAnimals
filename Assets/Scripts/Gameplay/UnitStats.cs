﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStats : MonoBehaviour {

    public bool meleeAttacker = true;
    public bool rangedAttacker = false;

    public float moveSpeed = 1f;
    public float meleeAttackSpeed = 1f;
    public float meleeAttackRange = 1f;
    public float rangedAttackSpeed = 1f;
    public float rangedAttackRange = 1f;

    public int hitPoints = 200;
    public int meleeDamage = 10;
    public int rangedDamage = 10;
    public int dodgeChance = 5;
       
	void Update ()
    {
        if(hitPoints <= 0)
        {
            Destroy(gameObject);
        }
        
        	
	}
}
