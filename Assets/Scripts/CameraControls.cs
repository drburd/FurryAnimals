﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour {

    public float panSpeed = 30f;
    public float edgeScrollZone = 0.01f;
    public float zoomRangeMin = 30f;
    public float zoomRangeMax = 50f;


    private float cameraMoveHorizontal;
    private float cameraMoveVertical;
    private float cameraZoom = 0;
    
    

    void Start ()
    {

    }
	
	
	void Update ()
    {
       cameraMoveHorizontal = Input.GetAxis("Horizontal");
       cameraMoveVertical = Input.GetAxis("Vertical");
       cameraZoom = Input.GetAxis("MouseScrollWheel");

        // Camera Panning
        if (cameraMoveHorizontal < 0)
        {
            transform.Translate(Vector3.left * Time.deltaTime * panSpeed, Space.World);
        }
        if (cameraMoveHorizontal > 0)
        {
            transform.Translate(Vector3.right * Time.deltaTime * panSpeed, Space.World);
        }

        if (cameraMoveVertical < 0)
        {
            transform.Translate(Vector3.back * Time.deltaTime * panSpeed, Space.World);
        }
        if (cameraMoveVertical > 0)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * panSpeed, Space.World);
        }


        // Camera Zoom
        cameraZoom = Input.GetAxis("MouseScrollWheel") * Time.deltaTime * 1000;
        //transform.Translate(Vector3.down * cameraZoom);
		transform.Translate(Camera.main.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2)).direction * cameraZoom, Space.World);
		transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, zoomRangeMin, zoomRangeMax),transform.position.z);
    }
}
