﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CreateUnitButtons : MonoBehaviour, IPointerDownHandler {

    public GameObject chosenPrefab;

    private Vector3 treeTransform;
    private Vector3 spawnPointTransform;
    
    void Start ()
    {
           
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        // Spawn unit in relation to TreeOfLife game object

        treeTransform = GameObject.Find("TreeOfLife").transform.position;

        spawnPointTransform.x = treeTransform.x += 10;
        spawnPointTransform.y = treeTransform.y -= 9;
        spawnPointTransform.z = treeTransform.z += 10;

        Instantiate(chosenPrefab, spawnPointTransform, transform.rotation);


    }

    void Update ()
    {
		
	}
}
