﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatDamageText : MonoBehaviour {

    public GameObject combatDamageContainer;

    private RectTransform tempRect;
    private Transform canvasParent;

    void Start ()
    {		
	}

    public void initCombatDamageText(string text)
    {
        GameObject temp = Instantiate(combatDamageContainer) as GameObject;
        tempRect = temp.GetComponent<RectTransform>();
        canvasParent = gameObject.transform.Find("Enemy_UI_Canvas");

        temp.transform.SetParent(canvasParent.transform);

        tempRect.transform.localPosition = combatDamageContainer.transform.localPosition;
        tempRect.transform.localScale = combatDamageContainer.transform.localScale;
        tempRect.transform.localRotation = combatDamageContainer.transform.localRotation;

        Vector2 randomPos = new Vector2(tempRect.transform.localPosition.x + Random.Range(-1f, 1f), tempRect.transform.localPosition.y + Random.Range(30f, 100f)); // + Random.Range(30f, 100f)
        tempRect.transform.localPosition = randomPos;
        

        temp.GetComponent<Text>().text = text;
        
        if (text == "Dodge!")
        {
            temp.GetComponent<Text>().color = new Color(255, 255, 255, 255);
            temp.GetComponent<Text>().fontSize = 15;
        }
            
        temp.GetComponent<Animator>().SetTrigger("Hit");
        
        Destroy(temp.gameObject, 2);

    
    }

    void Update ()
    {
		
	}
}
