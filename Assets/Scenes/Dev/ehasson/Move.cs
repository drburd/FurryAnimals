﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Move : MonoBehaviour {

	public string nameOfTagForWaypoint = null;
	private NavMeshAgent agent;
	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent> ();
		// This is a default way to say that object this script is attached to won't move to a waypoint when Start() is called
		if (nameOfTagForWaypoint == null || nameOfTagForWaypoint == "") {
			return;
		}

		GameObject spawnLocationGameObject = GameObject.FindGameObjectWithTag(nameOfTagForWaypoint);
		if (spawnLocationGameObject == null) {
			Debug.LogError ("You may not have a spawn location object in your scene tagged with: " + nameOfTagForWaypoint);
			return;
		}

		MoveUnit (spawnLocationGameObject.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void MoveUnit(Vector3 point){
		agent.destination = point;
	}

}
